import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { RecipePuppyAPI } from '../../shared/services/recipe-puppy-api.service';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class RecipesService {
    public requestURL: string;

    constructor(public http: Http, public recipePuppyAPI: RecipePuppyAPI) {
        this.requestURL = this.recipePuppyAPI.recipePuppyAPIConnect();
    }

    getAllRecipies(): Observable<any> {
        //Service to get all recipies from source url or api
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this.requestURL, options)
            .pipe(map((response: Response) => response.json()),
                catchError(this.handleError));
    }

    //Service method to handle and filter http errors
    public handleError(error: any): any {
        return Observable.throw(error.json());
    }
}