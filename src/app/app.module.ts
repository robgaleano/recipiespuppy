import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//Routing
import { AppRoutingModule } from '../modules/routing.module'
//Components
import { AppComponent } from './app.component';
import { RecipeFinderComponent } from '../pages/recipe-finder/recipe-finder.component';
import { PageNotFoundComponent } from '../pages/page-not-found/page-not-found.component';
//Services
import { RecipesService } from "../services/recipes-service/recipes.service";
import { RecipePuppyAPI } from "../shared/services/recipe-puppy-api.service";
//Modules
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatCardModule, MatButtonModule,MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
//Pipes
import { TextFilterPipe } from "../pages/recipe-finder/recipe-finder.pipe";

@NgModule({
  declarations: [
    AppComponent,
    RecipeFinderComponent,
    PageNotFoundComponent,
    TextFilterPipe
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [
    RecipePuppyAPI,
    RecipesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
