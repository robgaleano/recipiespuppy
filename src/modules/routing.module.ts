import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipeFinderComponent } from "../pages/recipe-finder/recipe-finder.component";
import { PageNotFoundComponent } from "../pages/page-not-found/page-not-found.component";

const appRoutes: Routes = [
    //More routes can be added if needed
    {
        path: 'recipe-finder',
        component: RecipeFinderComponent,
        data: { title: 'Recipes List' }
    },
    {
        path: '',
        redirectTo: '/recipe-finder',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }