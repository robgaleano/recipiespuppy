import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'recipefilter',
    pure: false
})
export class TextFilterPipe implements PipeTransform {
    transform(items: any[], searchText: string): any[] {
        //Get list of items and compare with searchtext in realtime
        if (!items) return [];
        if (!searchText) return items;
        searchText = searchText.toLowerCase();
        return items.filter(item => {
            //Then after the filter only return items that match with the query
            return item.title.toLowerCase().includes(searchText);
        });
    }
}