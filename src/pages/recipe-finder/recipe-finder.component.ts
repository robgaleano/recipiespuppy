import { Component } from '@angular/core';
import { RecipesService } from "../../services/recipes-service/recipes.service";

@Component({
  selector: 'app-recipe-finder',
  templateUrl: './recipe-finder.component.html',
  styleUrls: ['./recipe-finder.component.css']
})
export class RecipeFinderComponent {

  public recipePuppyDataList:any = [];
  public searchText:string;

  constructor(public recipesService:RecipesService) { 
    //Call service to retrieve data
    this.recipesService.getAllRecipies().subscribe(response=>{
      console.log(response.results);
      //Fill list with items
      this.recipePuppyDataList = response.results;
    },(err)=>{
      console.log(err);
      //Clear list if any errors retrieving data
      this.recipePuppyDataList = [];
    })
  }
}
