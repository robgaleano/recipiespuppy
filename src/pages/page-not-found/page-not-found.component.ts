import { Component, AfterViewInit, ElementRef } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements AfterViewInit {
  constructor(public elementRef: ElementRef){
  }

  ngAfterViewInit(){
    //Stablish base color to match image background
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#F6F6F6';
 }

}
