import { Injectable } from '@angular/core';

@Injectable()
export class RecipePuppyAPI {
    //Manage multiple API or BD conections
    public recipePuppyAPI:string = 'http://www.recipepuppy.com/api/';
    constructor() { }

    recipePuppyAPIConnect(){
        return this.recipePuppyAPI;
    }
    
}